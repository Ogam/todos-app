import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import "./modal";

const Index = () => {
  //hooks

  var [tasks, setTask] = useState([]);
  var [title, setTitle] = useState("");
  var [body, setBody] = useState("");

  //add task function
  var addTask = (e) => {
    e.preventDefault();
    setTask([...tasks, { title, body }]);
    setTitle("");
    setBody("");
  };
  //remove task
  var removeTask = (title) => {
    setTask(tasks.filter((task) => task.title !== title));
  };
  //show random tasks
  var randomTaskPick = () => {
    var randomPick = Math.floor(Math.random() * tasks.length);
    var pick = tasks[randomPick].body;
    alert(pick);
  };
  //changes a JSON string to noraml array and loads it in the browser
  useEffect(() => {
    var taskInfo = JSON.parse(localStorage.getItem("tasks"));
    if (taskInfo) {
      setTask(taskInfo);
    }
  }, []);
  //set items into local storage inform of a JSON string
  useEffect(() => {
    localStorage.setItem("tasks", JSON.stringify(tasks));
  }, [tasks]);
  return (
    <div>
      <h1>Task App</h1>
      <button onClick={randomTaskPick}>Show task</button>
      {tasks.map((task, index) => (
        <div key={task.title}>
          <h3>
            {index + 1}. {task.title}
          </h3>
          <p>{task.body}</p>
          <button onClick={() => removeTask(task.title)}>remove</button>
        </div>
      ))}
      <form onSubmit={addTask}>
        <input
          type="text"
          placeholder="task title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <textarea
          type="text"
          placeholder="task body"
          value={body}
          onChange={(e) => setBody(e.target.value)}
        ></textarea>
        <button>add task</button>
      </form>
    </div>
  );
};

ReactDOM.render(<Index />, document.getElementById("root"));
