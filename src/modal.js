import React from "react";
import Modal from "react-modal";

const ModalItem = () => {
  return (
    <Modal
      isOpen={modalIsOpen}
      onAfterOpen={afterOpenModal}
      onRequestClose={closeModal}
      style={customStyles}
      contentLabel="Example Modal"
    >
      <h3 className="modal__title">Selected Item</h3>

      <button className="button">Okay</button>
    </Modal>
  );
};

export default ModalItem;
